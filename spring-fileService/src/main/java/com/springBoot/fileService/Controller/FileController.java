package com.springBoot.fileService.Controller;

import com.springBoot.fileService.util.FileUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2018/12/4 9:26
 * @history: 1.2018/12/4 created by chuhaitao
 */
@RestController
@RequestMapping("/api/filedata/file")

public class FileController {

    /**
     * 只上传文件
     *
     * @param file
     * @param uploadFilePath
     * @return
     */
    public String justUploadAttach(MultipartFile file, String uploadFilePath) {
        File uploadFile = new File(uploadFilePath);
        //判断文件是否存在
        if (!uploadFile.exists()) {
            uploadFile.mkdirs();
        }
        //获取文件的存储名称
        String UUIDFileName = FileUtil.getFileUUIDName(file);
        //文件流的拷贝 拷贝到当前系统指定目录下的
        try {
            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(new File(uploadFilePath + "/" + UUIDFileName)));
        } catch (IOException e) {
            throw new RuntimeException("文件上传失败");
        }
        return UUIDFileName;
    }




    /**
     * 文件的下载
     *
     * @return
     */
    @ApiOperation("文件的下载")
    @GetMapping("/download")
    @ApiImplicitParam(name = "attachId", value = "主键", paramType = "body", dataType = "数字", required = true)
    public void downloadAttachment(@RequestParam("attachId") Integer attachId,
                                   HttpServletResponse httpServletResponse) {

        String filePath = "";
        File file = new File(filePath);
        FileUtil.download(httpServletResponse, file, "");
    }



}
