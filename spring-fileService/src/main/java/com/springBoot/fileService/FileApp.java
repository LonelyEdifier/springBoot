package com.springBoot.fileService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/4/17 17:15
 * @history: 1.2019/4/17 created by chuhaitao
 */
@SpringBootApplication
public class FileApp  extends SpringBootServletInitializer{

    public static void main(String[] args) {
        SpringApplication.run(FileApp.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(FileApp.class);
    }

}
