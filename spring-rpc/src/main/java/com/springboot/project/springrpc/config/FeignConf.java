package com.springboot.project.springrpc.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description: 配置feign的配置信息
 * @author: chuhaitao
 * @since: 2019/1/10 9:42
 * @history: 1.2019/1/10 created by chuhaitao
 */
@Configuration
public class FeignConf {

    /**
     * 配置feign接口调用打印的日志的级别
     *
     * @return
     */
    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }

}
