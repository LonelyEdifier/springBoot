package com.springboot.project.springrpc.service;

import com.springboot.project.springrpc.bean.RpcResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/2/12 18:05
 * @history: 1.2019/2/12 created by chuhaitao
 */
@FeignClient(
        name = "rpcService",
        url = "${rpc.server}",
        fallback = RpcService.RpcServiceFallBackImpl.class
)
public interface RpcService {

    /**
     * 发起远程过程调用
     *
     * @param request
     * @return
     */
    @RequestMapping("/****/****")
    RpcResult getRpcMethod(@RequestBody RpcResult request);


    /**
     * 本地降级的实现类
     */
    @Component
    class RpcServiceFallBackImpl implements RpcService {


        /**
         * 接口调用失败，统一调用本地的接口,自定义逻辑
         */
        @Override
        public RpcResult getRpcMethod(RpcResult request) {

            RpcResult obj = new RpcResult();

            return obj;
        }
    }

}
