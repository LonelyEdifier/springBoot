package com.springboot.project.springrpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;

/**
 * 开启FeignClient
 */
@EnableFeignClients
@SpringBootApplication
public class SpringRpcApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringRpcApplication.class, args);
    }

}

