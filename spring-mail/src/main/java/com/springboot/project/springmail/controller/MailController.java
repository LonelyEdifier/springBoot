package com.springboot.project.springmail.controller;

import com.springboot.project.springmail.bean.Mail;
import com.springboot.project.springmail.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/2/12 17:06
 * @history: 1.2019/2/12 created by chuhaitao
 */
@Controller
public class MailController {

    @Autowired
    MailService service;

    @PostMapping("/send")
    public void send(@RequestBody Mail mail) {
        service.send(mail);
    }

}
