package com.springboot.project.springmail.bean;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description: 封装邮件的pojo
 * @author: chuhaitao
 * @since: 2019/2/12 16:58
 * @history: 1.2019/2/12 created by chuhaitao
 */
@Setter
@Getter
public class Mail {
    /**
     * 发送人
     */

    private String[] recipients;
    /**
     * 主题
     */

    private String subject;
    /**
     * 内容
     */

    private String content;
    /**
     * 抄送人
     */

    private String[] ccPersons;
    /**
     * 附件列表
     */

    private List<Attach> attaches;


}
