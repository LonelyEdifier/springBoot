package com.springboot.project.springmail.bean;



import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description: 封装附件信息的pojo
 * @author: chuhaitao
 * @since: 2019/2/12 16:57
 * @history: 1.2019/2/12 created by chuhaitao
 */
@Setter
@Getter
public class Attach {



    private Long attachId;


    private String attachName;



    private String mime;



    private String extension;


    private String url;


    private String path;


    private String storedAttachName;



    private String attachComments;


    private String projectName;


    private Long projectId;


    private Long collectionId;


    private Date creationDate;



}
