package com.spring.common.util;

import com.cscec.financial.bean.common.pojo.UserInfo;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/3/7 15:58
 * @history: 1.2019/3/7 created by chuhaitao
 */
public class CookieUtil {

    private static final int COOKIE_MAX_AGE = 6 * 24 * 3600;
    private static final int COOKIE_MAX_HOUR = 30 * 60;


    /**
     * 放入cookie
     *
     * @param userSession
     * @param response
     */
    public static void setUserSessionToCookie(UserInfo userSession, HttpServletResponse response) {

      //  setCookie("userId", userSession.getUserId(), 0, response);
      //  Boolean boo = userSession.f();
       // setCookie("financialManager", boo.toString(), 0, response);

    }

    /**
     * 从cookie中获取用户的信息
     *
     * @param request
     * @return
     */
    public static UserInfo getUserSessionFormCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies == null || cookies.length < 1) {
            return null;
        }

        UserInfo userInfo = new UserInfo();
        PropertyDescriptor[] pds = null;
        try {
            BeanInfo beanInfo = Introspector.getBeanInfo(UserInfo.class, Object.class);
            pds = beanInfo.getPropertyDescriptors();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        if (pds == null) {
            return null;
        }
        for (PropertyDescriptor descriptor : pds) {
            String propertyName = descriptor.getName();
            Method setter = descriptor.getWriteMethod();
            String propertyType = descriptor.getPropertyType().getName();
            try {
                for (Cookie cookie : cookies) {
                    if (propertyName.equals(cookie.getName())) {
                        String value = cookie.getValue();

                        if ("boolean".equals(propertyType)) {
                            if ("true".equals(value)) {
                                setter.invoke(userInfo, true);
                            } else {
                                setter.invoke(userInfo, false);
                            }
                        } else {
                            setter.invoke(userInfo, value == null ? "" : value);

                        }
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return userInfo;

    }


    /**
     * 添加cookie
     * 单位时秒
     *
     * @param name
     * @param value
     * @param maxValue
     * @param response
     */
    public static void setCookie(String name, String value, int maxValue, HttpServletResponse response) {


        if (StringUtils.isEmpty(name)) {
            return;
        }
        if (StringUtils.isEmpty(value)) {
            value = "";
        }

        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");

        if (maxValue != 0) {
            cookie.setMaxAge(maxValue);
        } else {
            cookie.setMaxAge(COOKIE_MAX_HOUR);
        }
        response.addCookie(cookie);

       /* try {
         //   response.flushBuffer();
        } catch (IOException e) {
            e.printStackTrace();
        }*/


    }


    /**
     * 获取Cookie
     *
     * @param request
     * @param name
     * @return
     */
    public Cookie getCookie(HttpServletRequest request, String name) {

        Cookie[] cookies = request.getCookies();
        if (cookies == null || cookies.length < 1) {
            return null;
        }
        Cookie cookie = null;
        for (Cookie c : cookies) {
            if (name.equals(c.getName())) {
                cookie = c;
                break;
            }
        }
        return cookie;
    }
}
