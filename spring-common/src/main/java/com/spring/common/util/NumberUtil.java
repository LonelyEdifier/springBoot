package com.spring.common.util;

import java.text.DecimalFormat;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/4/10 11:06
 * @history: 1.2019/4/10 created by chuhaitao
 */
public class NumberUtil {


    public static Double decimalFormat(Double number, String format) {

        DecimalFormat df = new DecimalFormat(format);
        String numberStr = df.format(number);
        Double newDouble = Double.parseDouble(numberStr);
        return newDouble;
    }

    public static void main(String[] args) {
        Double a = 0.0;
        System.out.println(decimalFormat(a, "0.00"));
    }

}
