package com.spring.common.util;

import org.springframework.util.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:
 * @author: chuhaitao
 * @since: 2019/1/31 9:20
 * @history: 1.2019/1/31 created by chuhaitao
 */
public class DateUtil {


    private static DateFormat df = new SimpleDateFormat("yyyy/MM/dd");


    public static String getDateStr(Date date, DateFormat df) {
        return df.format(date);
    }

    /**
     * 获取当前年的integer类型
     *
     * @param date
     * @return
     */
    public static Integer parseDateToYearInt(Date date) {
        DateFormat df = new SimpleDateFormat("yyyy");
        return Integer.parseInt(df.format(date));
    }

    public static Date getDateByFormat(Date date, String format) {
        if (date == null) {
            return null;
        }
        DateFormat df = new SimpleDateFormat(format);
        Date newDate = null;
        try {
            newDate = df.parse(df.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return newDate;
    }

    public static Date dateFormat(Date date, String format) {
        if (StringUtils.isEmpty(format)) {
            format = "yyyy-MM-dd";
        }
        DateFormat df = new SimpleDateFormat(format);
        Date newDate = null;
        if (!StringUtils.isEmpty(date)) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 1);
            try {
                newDate = df.parse(df.format(calendar.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return newDate;
    }

    /**
     * 获取下一天
     *
     * @param date
     * @return
     */
    public static Date getNextDay(Date date) {
        Date nextDay = null;
        if (!StringUtils.isEmpty(date)) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
            try {
                nextDay = df.parse(df.format(calendar.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return nextDay;
    }

    public static Date getPreDay(Date date) {
        if (date == null) {
            return null;
        }
        Date nextDay = null;
        if (!StringUtils.isEmpty(date)) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) - 1);
            try {
                nextDay = df.parse(df.format(calendar.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return nextDay;
    }


    /**
     * 获取上一年
     *
     * @param date
     * @return
     */
    public static Date getLastYear(Date date) {
        Date lastYear = null;
        if (!StringUtils.isEmpty(date)) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.DATE, calendar.get(Calendar.YEAR) - 1);
            try {
                lastYear = df.parse(df.format(calendar.getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return lastYear;
    }


    /**
     * 获取本年最后一天
     *
     * @return
     */
    public static Date getYearEnd(String format) {
        DateFormat df = new SimpleDateFormat(format);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, calendar.getActualMaximum(Calendar.MONTH));
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date currYearLast = calendar.getTime();

        Date yearEnd = null;

        try {
            yearEnd = df.parse(df.format(currYearLast));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return yearEnd;
    }


    public static Long getDaySub(Date endDate, Date startDate) {
        Long days = 0L;
        if (endDate == null || startDate == null) {
            return days;
        }
        try {

            Long end = df.parse(df.format(endDate)).getTime();
            Long star = df.parse(df.format(startDate)).getTime();
            days = (end - star) / (24 * 60 * 60 * 1000);
            ;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return days;
    }

    public static void main(String[] args) {

        System.out.println(DateUtil.getDateByFormat(DateUtil.getNextDay(new Date()), "yyyy/MM/dd"));

    }
}
