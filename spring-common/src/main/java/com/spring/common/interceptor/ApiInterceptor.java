package com.spring.common.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:

 * @author: chuhaitao
 * @since: 2019/3/4 11:22
 * @history: 1.2019/3/4 created by chuhaitao
 */
public class ApiInterceptor implements HandlerInterceptor {

    /**
     * 1、拦截header信息
     * 2、用来判断是否是海外接口
     * 3、获取用户的一些信息
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {


        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView
            modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception
            ex) throws Exception {



    }


}
