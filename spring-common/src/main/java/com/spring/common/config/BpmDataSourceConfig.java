package com.spring.common.config;


import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

import javax.sql.DataSource;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description:配置bpm审批历史的数据源
 * @author: chuhaitao
 * @since: 2019/1/22 9:27
 * @history: 1.2019/1/22 created by chuhaitao
 */
@Configuration
@MapperScan(basePackages = "com.cscec.financial.bpm.dao", sqlSessionTemplateRef = "sqlSessionTemplate_two")
public class BpmDataSourceConfig {

    Logger logger = LoggerFactory.getLogger(BpmDataSourceConfig.class);

    /**
     * 本地环境的bpm的数据源
     *
     * @return
     */
    @Profile("local")
    @Bean(name = "bpmDataSource")
    @Qualifier("bpmDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.bpm")
    public DataSource bpmDataSource_local() {
       return DataSourceBuilder.create().build();
    }
    /**
     * 开发环境的bpm的数据源
     *
     * @return
     */
    @Profile("dev")
    @Bean(name = "bpmDataSource")
    @Qualifier("bpmDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.bpm")
    public DataSource bpmDataSource_dev() {
        JndiDataSourceLookup jndiDataSourceLookup = new JndiDataSourceLookup();
        DataSource dataSource = jndiDataSourceLookup.getDataSource("jdbc/BPMConnDS");
        logger.info("--初始化webLogic数据源,jndiName:jdbc/BPMConnDS");
        return dataSource;
    }

    /**
     * 测试环境的bpm数据源
     *
     * @return
     */
    @Profile("test")
    @Bean(name = "bpmDataSource")
    @Qualifier("bpmDataSource")
    public DataSource bpmDataSource_test() {
        JndiDataSourceLookup jndiDataSourceLookup = new JndiDataSourceLookup();
        DataSource dataSource = jndiDataSourceLookup.getDataSource("jdbc/FISConnDS");
        logger.info("--初始化webLogic数据源,jndiName:jdbc/FISConnDS ");
        return dataSource;
    }


    /**
     * 测试环境的bpm数据源
     *
     * @return
     */
    @Profile("pro")
    @Bean(name = "bpmDataSource")
    @Qualifier("bpmDataSource")
    public DataSource bpmDataSource_pro() {
        JndiDataSourceLookup jndiDataSourceLookup = new JndiDataSourceLookup();
        DataSource dataSource = jndiDataSourceLookup.getDataSource("jdbc/FISConnDS");
        logger.info("--初始化webLogic数据源,jndiName:jdbc/FISConnDS ");
        return dataSource;
    }

    /**
     * 配置sqlsession
     *
     * @param dataSource
     * @return
     * @throws Exception
     */
    @Bean(name = "sqlSessionFactory_two")
    public SqlSessionFactory sqlSessionFactory_two(@Qualifier("bpmDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:bpm/*.xml"));
        return sqlSessionFactoryBean.getObject();
    }

    /**
     * 配置事务管理器
     *
     * @param dataSource
     * @return
     */
    @Bean(name = "TransactionManager_two")
    public DataSourceTransactionManager TransactionManager_two(@Qualifier("bpmDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    /**
     * 配置SqlSessionTemplate
     *
     * @param sqlSessionFactory
     * @return
     */
    @Bean(name = "sqlSessionTemplate_two")
    public SqlSessionTemplate sqlSessionTemplate_two(@Qualifier("sqlSessionFactory_two") SqlSessionFactory sqlSessionFactory) {
        SqlSessionTemplate sqlSessionTemplate =
                new SqlSessionTemplate(sqlSessionFactory);
        return sqlSessionTemplate;
    }


}
