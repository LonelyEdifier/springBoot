package com.spring.common.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

import javax.sql.DataSource;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description: 自定义数据源
 * @author: chuhaitao
 * @since: 2018/11/27 18:53
 * @history: 1.2018/11/27 created by chuhaitao
 */
@Configuration
@MapperScan(basePackages = "com.cscec.financial.dao", sqlSessionTemplateRef = "sqlSessionTemplate_one")
public class FisDataSource {

    Logger logger = LoggerFactory.getLogger(FisDataSource.class);

    @Value("${jndi.name}")
    private String jndiName;

    /**
     * dev数据源
     *
     * @return
     */
    @Profile("local")
    @Bean(name = "primaryDataSource")
    @Qualifier("primaryDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.FIS")
    public DataSource dataSource_one_local() {
     return DataSourceBuilder.create().build();
    }

    /**
     * dev数据源
     *
     * @return
     */
    @Profile("dev")
    @Bean(name = "primaryDataSource")
    @Qualifier("primaryDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.FIS")
    public DataSource dataSource_one_dev() {
        JndiDataSourceLookup jndiDataSourceLookup = new JndiDataSourceLookup();
        DataSource dataSource = jndiDataSourceLookup.getDataSource("jdbc/FISConnDS");
        logger.info("--初始化webLogic数据源,jndiName:jdbc/FISConnDS");
        return dataSource;
    }

    /**
     * test环境配置数据源
     *
     * @return
     */
    @Profile("test")
    @Bean(name = "primaryDataSource")
    @Qualifier("primaryDataSource")
    @Primary
    public DataSource dataSource_one_test() {
        JndiDataSourceLookup jndiDataSourceLookup = new JndiDataSourceLookup();
        DataSource dataSource = jndiDataSourceLookup.getDataSource("jdbc/FISConnDS");
        logger.info("--初始化webLogic数据源,jndiName:jdbc/FISConnDS ");
        return dataSource;
    }

    /**
     * pro环境配置数据源
     *
     * @return
     */
    @Profile("pro")
    @Bean(name = "primaryDataSource")
    @Qualifier("primaryDataSource")
    @Primary
    public DataSource dataSource_one_pro() {
        JndiDataSourceLookup jndiDataSourceLookup = new JndiDataSourceLookup();
        DataSource dataSource = jndiDataSourceLookup.getDataSource(jndiName);
        logger.info("--初始化webLogic数据源,jndiName:" + jndiName);
        return dataSource;
    }

    /**
     * 配置sqlsession
     *
     * @param dataSource
     * @return
     * @throws Exception
     */
    @Bean(name = "sqlSessionFactory_one")
    @Primary
    public SqlSessionFactory sqlSessionFactory_one(@Qualifier("primaryDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(dataSource);
        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml"));
        return sqlSessionFactoryBean.getObject();
    }

    /**
     * 配置事务管理器
     *
     * @param dataSource
     * @return
     */
    @Bean(name = "TransactionManager_one")
    @Primary
    public DataSourceTransactionManager TransactionManager_one(@Qualifier("primaryDataSource") DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    /**
     * 配置SqlSessionTemplate
     *
     * @param sqlSessionFactory
     * @return
     */
    @Bean(name = "sqlSessionTemplate_one")
    @Primary
    public SqlSessionTemplate sqlSessionTemplate_one(@Qualifier("sqlSessionFactory_one") SqlSessionFactory sqlSessionFactory) {
        SqlSessionTemplate sqlSessionTemplate =
                new SqlSessionTemplate(sqlSessionFactory);
        return sqlSessionTemplate;
    }


}
