package com.spring.common.config;

import com.spring.common.interceptor.ApiInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @Copyright: Shanghai Definesys Company.All rights reserved.
 * @Description: 配置拦截器的拦截路径 和 不拦截的路径
 * @author: chuhaitao
 * @since: 2019/3/7 16:54
 * @history: 1.2019/3/7 created by chuhaitao
 */
@Configuration
public class ApiInterceptorConf extends WebMvcConfigurerAdapter {


    private static String[] addPathUrls = new String[]{
            "/api/rpiModel/rpi/approve",
            "/api/rpiModel/rpi/export"

    };

    private static String[] excludeUrls = new String[]{
            "/api/bpm/**"

    };


    @Bean
    public ApiInterceptor getApiInterceptor() {
        return new ApiInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(getApiInterceptor())
                .addPathPatterns(addPathUrls)
                .excludePathPatterns(excludeUrls)


        ;

    }

}
